#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"
#include "ofAppNoWindow.h"

//#define NO_WIDOW 1

//========================================================================
int main( ){

#ifdef NO_WIDOW
    ofAppNoWindow window;
    ofSetupOpenGL( &window, 1920, 480, OF_WINDOW);			// <-------- setup the GL context
#else
    ofAppGlutWindow window;
    ofSetupOpenGL( &window, 1920, 480, OF_WINDOW);			// <-------- setup the GL context
#endif    
    
    // this kicks off the running of my app
    // can be OF_WINDOW or OF_FULLSCREEN
    // pass in width and height too:
    ofRunApp( new testApp());

}
