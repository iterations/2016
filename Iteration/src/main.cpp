#include "ofMain.h"
#include "testApp.h"
#include "ofAppGlutWindow.h"
#include "ofAppNoWindow.h"
#include "Config.h"

#define NO_WIDOW 1

//========================================================================

int main( int argc, char *argv[] ) {

    std::cout << "iterations 2016 man" << std::endl;
    std::cout << "\t'-nonet' to disable network" << std::endl;
    std::cout << "\t'-n [string]' to set the name of the point" << std::endl;
    std::cout << "\t'-p [int]' to set local port, 23000 by default" << std::endl;
    std::cout << "\t'-b [int]' to set broadcast port, 20000 by default" << std::endl;
    std::cout << "\t'-s [int]x[int]' to set the camera resolution, 160x120 by default" << std::endl;
    std::cout << "\t'-t [int]' to set the threshold, 85 by default" << std::endl;
    std::cout << "\t'-f [string|string]' to define the names of points listened" << std::endl;
    std::cout << "\t'-verbose" << std::endl;
    for ( int i = 0; i < argc; ++i ) {
        std::string param = argv[i];
        if ( param.compare( "-p" ) == 0 && i < argc - 1 ) {
            int port = atoi( argv[i+1] );
            std::cout << "--local port: " << port << std::endl;
            Config::get()->osc_port = port;
        } else if ( param.compare( "-t" ) == 0 && i < argc - 1 ) {
            int threshold = atoi( argv[i+1] );
            std::cout << "--threshold: " << threshold << std::endl;
            Config::get()->threshold = threshold;
        } else if ( param.compare( "-b" ) == 0 && i < argc - 1 ) {
            int port = atoi( argv[i+1] );
            std::cout << "--broadcast port: " << port << std::endl;
            Config::get()->broadcast_port = port;
        } else if ( param.compare( "-n" ) == 0 && i < argc - 1 ) {
            std::cout << "--name: " << argv[i+1] << std::endl;
            Config::get()->name = argv[i+1];
        } else if ( param.compare( "-f" ) == 0 && i < argc - 1 ) {
            std::cout << "--filter: " << argv[i+1] << std::endl;
            Config::get()->names_filter = argv[i+1];
        } else if ( param.compare( "-s" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            // spliting the string on 'x'
            size_t xpos = param.find( "x" );
            if ( xpos != string::npos ) {
                Config::get()->cam_width = atoi( param.substr( 0, xpos ).c_str() );
                Config::get()->cam_height = atoi( param.substr( xpos + 1 ).c_str() );
                std::cout << "--size: " << Config::get()->cam_width << "x" << Config::get()->cam_height << std::endl;
            }
        } else if ( param.compare( "-verbose" ) == 0 ) {
            Config::get()->verbose = true;
        } else if ( param.compare( "-nonet" ) == 0 ) {
            Config::get()->enable_osc = false;
        }
    }

#ifdef NO_WIDOW
    ofAppNoWindow window;
    ofSetupOpenGL( &window, 1920, 480, OF_WINDOW);
#else
    //ofAppGlutWindow window;
    ofSetupOpenGL( 640, 480, OF_WINDOW );
#endif

    ofRunApp( new testApp( ) );

}
