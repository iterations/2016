#ifndef _TEST_APP
#define _TEST_APP

#include "ofxOpenCv.h"
#include "ofxARToolkitPlus.h"

#include "ofMain.h"
#include "OSCAgent.h"

// Uncomment this to use a camera instead of a video file
#define CAMERA_CONNECTED

typedef uint16_t tagid;
typedef uint8_t tagcount;

class testApp : public ofBaseApp {

public:
    
    void setup();
    void update();
    void draw();
    void keyPressed(int key);

protected:
        
    int width, height;

#ifdef CAMERA_CONNECTED
    ofVideoGrabber vidGrabber;
#else
    ofVideoPlayer vidPlayer;
#endif

    ofxARToolkitPlus artk;
    ofxCvColorImage colorImage;
    ofxCvGrayscaleImage grayImage;
//    ofImage displayImage;
//    vector<ofPoint> displayImageCorners;

    /* previous tags memory */
    // <tag id, number of time it is seen>
    map< tagid, tagcount > previous_tags;
    
    OSCAgent * oscagent;

};

#endif
