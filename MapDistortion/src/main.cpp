#include "ofMain.h"
#include "ofApp.h"
#include "ofAppNoWindow.h"
#include "Config.h"
#include "MapConfig.h"

//#define NO_WIDOW 1

//========================================================================

int main( int argc, char *argv[] ) {

    std::cout << "oscagent 2016 man" << std::endl;
    std::cout << "\t'-nonet' to disable network" << std::endl;
    std::cout << "\t'-n [string]' to set the name of the point" << std::endl;
    std::cout << "\t'-p [int]' to set local port, 23000 by default" << std::endl;
    std::cout << "\t'-b [int]' to set broadcast port, 20000 by default" << std::endl;
    std::cout << "\t'-f [string|string]' to define the names of points listened" << std::endl;
    std::cout << "\t'-verbose" << std::endl;
    std::cout << "map distortion" << std::endl;
    std::cout << "\t'-cu [int],[int],[int]' to define UP color (RGBA), alpha is optional" << std::endl;
    std::cout << "\t'-cd [int],[int],[int]' to define DOWN color (RGBA), alpha is optional" << std::endl;
    std::cout << "\t'-g [int]' to define grid resolution" << std::endl;
    std::cout << "\t'-norm [int]' to define max value of tag positions, used to normalise" << std::endl;
    std::cout << "\t'-z [int]' to define min and max depth of the grid" << std::endl;
    std::cout << "\t'-smooth [float]' to define smoothing speed" << std::endl;
    std::cout << "\t'-wa [float]-[float]' set the wave amplitude range" << std::endl;
    std::cout << "\t'-wf [float]-[float]' set the frequency range" << std::endl;
    std::cout << "\t'-rad [float]-[float]' set the radius range" << std::endl;
    std::cout << "\t'-force [float]-[float]' set the force range" << std::endl;
    std::cout << "\t'-chance [float]' set the chance to change the map texture" << std::endl;
    std::cout << "\t'-rot [float]' set the rotation multiplier of the view" << std::endl;
    std::cout << "\t'-debug [int]' set to 1 to display debug" << std::endl;
    
    for ( int i = 0; i < argc; ++i ) {
        std::string param = argv[i];
        if ( param.compare( "-p" ) == 0 && i < argc - 1 ) {
            int port = atoi( argv[i+1] );
            std::cout << "--local port: " << port << std::endl;
            Config::get()->osc_port = port;
        } else if ( param.compare( "-t" ) == 0 && i < argc - 1 ) {
            int threshold = atoi( argv[i+1] );
            std::cout << "--threshold: " << threshold << std::endl;
            Config::get()->threshold = threshold;
        } else if ( param.compare( "-b" ) == 0 && i < argc - 1 ) {
            int port = atoi( argv[i+1] );
            std::cout << "--broadcast port: " << port << std::endl;
            Config::get()->broadcast_port = port;
        } else if ( param.compare( "-n" ) == 0 && i < argc - 1 ) {
            std::cout << "--name: " << argv[i+1] << std::endl;
            Config::get()->name = argv[i+1];
        } else if ( param.compare( "-f" ) == 0 && i < argc - 1 ) {
            std::cout << "--filter: " << argv[i+1] << std::endl;
            Config::get()->names_filter = argv[i+1];
        } else if ( param.compare( "-verbose" ) == 0 ) {
            Config::get()->verbose = true;
        } else if ( param.compare( "-nonet" ) == 0 ) {
            Config::get()->enable_osc = false;
        
        // MAP CONFIGURATION
        } else if ( param.compare( "-cu" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            vector<string> scol = MapConfig::split( param, ',' );
            if ( scol.size() == 3 || scol.size() == 4 ) {
                MapConfig::get()->up_red = atoi( scol[0].c_str() );
                MapConfig::get()->up_green = atoi( scol[1].c_str() );
                MapConfig::get()->up_blue = atoi( scol[2].c_str() );
                std::cout << "--up_color: " << MapConfig::get()->up_red << ", " << MapConfig::get()->up_green << ", " << MapConfig::get()->up_blue << std::endl;
            }
        } else if ( param.compare( "-cd" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            vector<string> scol = MapConfig::split( param, ',' );
            if ( scol.size() == 3 || scol.size() == 4 ) {
                MapConfig::get()->down_red = atoi( scol[0].c_str() );
                MapConfig::get()->down_green = atoi( scol[1].c_str() );
                MapConfig::get()->down_blue = atoi( scol[2].c_str() );
                std::cout << "--down_color: " << MapConfig::get()->down_red << ", " << MapConfig::get()->down_green << ", " << MapConfig::get()->down_blue << std::endl;
            }
        } else if ( param.compare( "-g" ) == 0 && i < argc - 1 ) {
            MapConfig::get()->grid_resolution = atoi( argv[i+1] );
            std::cout << "--grid_resolution: " << MapConfig::get()->grid_resolution << std::endl;
        } else if ( param.compare( "-norm" ) == 0 && i < argc - 1 ) {
            MapConfig::get()->position_max = atoi( argv[i+1] );
            std::cout << "--position_max: " << MapConfig::get()->position_max << std::endl;
        } else if ( param.compare( "-z" ) == 0 && i < argc - 1 ) {
            MapConfig::get()->z_range = atoi( argv[i+1] );
            std::cout << "--z_range: " << MapConfig::get()->z_range << std::endl;
        } else if ( param.compare( "-smooth" ) == 0 && i < argc - 1 ) {
            MapConfig::get()->smoothing = atof( argv[i+1] );
            std::cout << "--smoothing: " << MapConfig::get()->smoothing << std::endl;
        } else if ( param.compare( "-wa" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            vector<string> scol = MapConfig::split( param, '-' );
            if ( scol.size() == 2 ) {
                MapConfig::get()->wave_ampl_min = atof( scol[0].c_str() );
                MapConfig::get()->wave_ampl_max = atof( scol[1].c_str() );
                std::cout << "--wave_ampl_range: [" << MapConfig::get()->wave_ampl_min << ", " << MapConfig::get()->wave_ampl_max << "]" << std::endl;
            }
        } else if ( param.compare( "-wf" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            vector<string> scol = MapConfig::split( param, '-' );
            if ( scol.size() == 2 ) {
                MapConfig::get()->wave_freq_min = atof( scol[0].c_str() );
                MapConfig::get()->wave_freq_max = atof( scol[1].c_str() );
                std::cout << "--wave_frequency_range: [" << MapConfig::get()->wave_freq_min << ", " << MapConfig::get()->wave_freq_max << "]" << std::endl;
            }
        } else if ( param.compare( "-rad" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            vector<string> scol = MapConfig::split( param, '-' );
            if ( scol.size() == 2 ) {
                MapConfig::get()->radius_min = atof( scol[0].c_str() );
                MapConfig::get()->radius_max = atof( scol[1].c_str() );
                std::cout << "--radius_range: [" << MapConfig::get()->radius_min << ", " << MapConfig::get()->radius_max << "]" << std::endl;
            }
        } else if ( param.compare( "-force" ) == 0 && i < argc - 1 ) {
            param = argv[i+1];
            vector<string> scol = MapConfig::split( param, '-' );
            if ( scol.size() == 2 ) {
                MapConfig::get()->force_min = atof( scol[0].c_str() );
                MapConfig::get()->force_max = atof( scol[1].c_str() );
                std::cout << "--force_range: [" << MapConfig::get()->force_min << ", " << MapConfig::get()->force_max << "]" << std::endl;
            }
        } else if ( param.compare( "-chance" ) == 0 && i < argc - 1 ) {
            MapConfig::get()->chance_for_map = atof( argv[i+1] );
            std::cout << "--chance: " << MapConfig::get()->chance_for_map << std::endl;
        } else if ( param.compare( "-rot" ) == 0 && i < argc - 1 ) {
            MapConfig::get()->rotation_mult = atof( argv[i+1] );
            std::cout << "--rotation_mult: " << MapConfig::get()->rotation_mult << std::endl;
        } else if ( param.compare( "-debug" ) == 0 && i < argc - 1 ) {
            int d = atoi( argv[i+1] );
            if ( d == 1 ) {
                MapConfig::get()->display_debug = true;
            }
            std::cout << "--display_debug: " << MapConfig::get()->display_debug << std::endl;
        }
        
    }

#ifdef NO_WIDOW
    ofAppNoWindow window;
    ofSetupOpenGL( &window, 1920, 480, OF_WINDOW);
#else
    ofSetupOpenGL( 1280, 720, OF_FULLSCREEN );
#endif

    ofRunApp( new ofApp( ) );

}