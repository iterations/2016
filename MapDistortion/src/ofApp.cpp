#include <sstream>

#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

#ifdef TARGET_OPENGLES
    ofEnableNormalizedTexCoords();
#endif
    
    ofHideCursor();
    
    ofSetLogLevel( OF_LOG_FATAL_ERROR );
    
    if ( Config::get()->enable_osc ){    
        DataKind dk = OSCAgent::makeKind( false, false, false, false );
        oscagent = new OSCAgent( dk );
        oscagent->addListener( this );
    }
    
    bounds.init = false;
    btype = BRUSH_NONE;
    
    ofDirectory mdir( "maps" );
    if ( mdir.isDirectory() ) {
        vector< ofFile > mfiles = mdir.getFiles();
        for( vector< ofFile >::iterator itf = mfiles.begin(); itf != mfiles.end(); ++itf ) {
            maps.push_back( itf->getAbsolutePath() );
        }
    }
    
    if ( !maps.empty() ) {
        mapgrid.load( 
            maps[ 0 ], 
            MapConfig::get()->grid_resolution,
            MapConfig::get()->grid_resolution
        );
    }
    
}

//--------------------------------------------------------------
void ofApp::update(){

    if ( Config::get()->enable_osc ) {   
        
        long now = ofGetElapsedTimeMillis();
        
        oscagent->update( ofGetElapsedTimeMillis() );
        ofVec2f prev_barycenter;
        prev_barycenter = barycenter;
        barycenter.set( 0,0 );
        int alives = 0;
        vector< string > zombies;
        for ( map< string, BrushPoint >::iterator itp = bpoints.begin(); itp != bpoints.end(); ++itp ) {
            
            BrushPoint & bp = itp->second;
            
            if ( now - bp.lastseen > Config::get()->max_delay_millis ) {
                zombies.push_back( itp->first );
                continue;
            }
            
            alives++;
            
            switch( bp.type ) {
                case BRUSH_PUSH:
                    mapgrid.push( bp.pos.x, bp.pos.y, bp.radius, bp.force, bp.limit );
                    break;
                case BRUSH_PUSH_FORCE:
                    mapgrid.push( bp.pos.x, bp.pos.y, bp.radius, bp.force, bp.limit );
                    break;
                case BRUSH_PULL:
                    mapgrid.push( bp.pos.x, bp.pos.y, bp.radius, bp.force, bp.limit );
                    break;
                case BRUSH_PINCH:
                    mapgrid.pinch( bp.pos.x, bp.pos.y, bp.radius, bp.force );
                    break;
                case BRUSH_INFLATE:
                    mapgrid.pinch( bp.pos.x, bp.pos.y, bp.radius, bp.force );
                    break;
                default:
                    break;
            }
            barycenter += bp.pos;
        }
        
        if ( alives > 0 ) {
            barycenter /= alives;
            barycenter = prev_barycenter + ( ( barycenter - prev_barycenter ) * 0.2 );
        }
        
        for ( vector<string>::iterator itz = zombies.begin(); itz != zombies.end(); ++itz ) {
            bpoints.erase( (*itz) );
        }
    
    } else {
    
        ofVec2f mm(
            ofGetMouseX() - ofGetWidth() * 0.5,
            ofGetMouseY() - ofGetHeight() * 0.5
        );
        
        switch ( btype ) {
            case BRUSH_PUSH:
                mapgrid.push( mm.x, mm.y, 150, -0.04 );
                break;
            case BRUSH_PUSH_FORCE:
                mapgrid.push( mm.x, mm.y, 150, -0.04, false );
                break;
            case BRUSH_PULL:
                mapgrid.push( mm.x, mm.y, 150, 0.04, true );
                break;
            case BRUSH_PINCH:
                mapgrid.pinch( mm.x, mm.y, 150, 0.04 );
                break;
            case BRUSH_INFLATE:
                mapgrid.pinch( mm.x, mm.y, 250, -0.04 );
                break;
            default:
                mapgrid.setWave( 
                    ofMap( 
                        ofGetMouseX() * 1.f / ofGetWidth(), 0.f, 1.f, 
                        MapConfig::get()->wave_ampl_min,
                        MapConfig::get()->wave_ampl_max
                        ), 
                    ofMap( 
                        ofGetMouseY() * 1.f / ofGetHeight(), 0.f, 1.f, 
                        MapConfig::get()->wave_freq_min,
                        MapConfig::get()->wave_freq_max
                        )
                    );
//                barycenter = mm;
                break;

        }
        
        if ( btype != BRUSH_NONE ) {
            barycenter *= 0.1;
        } 
    
    }
    
    mapgrid.update();
    
    for ( map< string, Request >::iterator itr = requests.begin(); itr != requests.end(); ++itr ) {
        if ( itr->second.nexttime <= ofGetElapsedTimeMillis() ) {
            oscagent->request( itr->first, itr->second.req.i );
            requests[ itr->first ].nexttime = ofGetElapsedTimeMillis() + 5000;
        }
    }
    
}

void ofApp::process( ofxOscMessage & msg ) {
    
    if ( msg.getAddress().compare( OSC_ADDRESS_DATA ) == 0 ) {

        DataKind dk( msg.getArgAsInt( 0 ) );
        
        if ( dk[ 4 ] ) {
            
            string sender_name = msg.getArgAsString( 1 );
            float p = msg.getArgAsFloat( 2 );
            mapgrid.setWave( 
                    ofMap( 
                        p, 0.f, 1.f, 
                        MapConfig::get()->wave_ampl_min,
                        MapConfig::get()->wave_ampl_max
                        ), 
                    ofMap( 
                        p, 0.f, 1.f, 
                        MapConfig::get()->wave_freq_min,
                        MapConfig::get()->wave_freq_max
                        )
                    );
        
        } else if ( dk[ 0 ] ) {
        
            string sender_name = msg.getArgAsString( 1 );
            int tagid = msg.getArgAsInt( 2 );
            int event = msg.getArgAsInt( 3 );

            stringstream ss;
            ss << msg.getRemoteIp() << "-t-" << tagid;
            string utag = ss.str();
        
            if ( event == TAG_DISAPPEARED && bpoints.find( utag ) != bpoints.end() ) {
                
                bpoints.erase( utag );
//                cout << "brush point removed" <<endl;
                
            } else {
                
//                cout << "point received" <<endl;
                
                if ( event == TAG_APPEAR || bpoints.find( utag ) == bpoints.end() ) {
                
                    int btype = ofRandom( 1, (int) BRUSH_COUNT );
                    if ( btype == (int) BRUSH_COUNT ) {
                        btype = ofRandom( 1, ( (int) BRUSH_COUNT ) - 1 );
                    }
                    bpoints[ utag ].type = (BrushType) btype;
                    bpoints[ utag ].radius = ofRandom( MapConfig::get()->radius_min, MapConfig::get()->radius_max );
                    switch ( bpoints[ utag ].type ) {
                        case BRUSH_PUSH:
                            bpoints[ utag ].force = ofRandom( MapConfig::get()->force_min, MapConfig::get()->force_max );
                            bpoints[ utag ].limit = false;
                            break;
                        case BRUSH_PUSH_FORCE:
                            bpoints[ utag ].force = ofRandom( MapConfig::get()->force_min, MapConfig::get()->force_max );
                            bpoints[ utag ].limit = true;
                            break;
                        case BRUSH_PULL:
                            bpoints[ utag ].force = ofRandom( -MapConfig::get()->force_min, -MapConfig::get()->force_max );
                            bpoints[ utag ].limit = true;
                            break;
                        case BRUSH_PINCH:
                            bpoints[ utag ].force = ofRandom( MapConfig::get()->force_min, MapConfig::get()->force_max );
                            bpoints[ utag ].limit = false;
                            break;
                        case BRUSH_INFLATE:
                            bpoints[ utag ].force = ofRandom( -MapConfig::get()->force_min, -MapConfig::get()->force_max );
                            bpoints[ utag ].limit = false;
                            break;
                        default:
                            break;
                    }
                    
                    if ( ofRandomuf() > MapConfig::get()->chance_for_map ) {
                        int mp = ofRandom( 0, maps.size() );
                        if ( mp == maps.size() ) {
                            mp = ofRandom( 0, maps.size() - 1 );
                        }
                        mapgrid.loadMap( maps[ mp ] );
                    }
                    
                }
                
                bpoints[ utag ].lastseen = ofGetElapsedTimeMillis();
                
                bpoints[ utag ].pos.set(
                    msg.getArgAsFloat( 4 ),
                    msg.getArgAsFloat( 5 )
                );
                
                // mapping
                bpoints[ utag ].pos /= MapConfig::get()->position_max;
                bpoints[ utag ].pos *= mapgrid.getWidth() * 0.5;
                
                if ( !bounds.init ) {
                    
                    bounds.init = true;
                    bounds.min = bpoints[ utag ].pos;
                    bounds.max = bpoints[ utag ].pos;
                    
                } else {
                    
                    if ( bounds.min.x > bpoints[ utag ].pos.x ) {
                        bounds.min.x = bpoints[ utag ].pos.x;
                    }
                    if ( bounds.min.y > bpoints[ utag ].pos.y ) {
                        bounds.min.y = bpoints[ utag ].pos.y;
                    }
                    if ( bounds.max.x < bpoints[ utag ].pos.x ) {
                        bounds.max.x = bpoints[ utag ].pos.x;
                    }
                    if ( bounds.max.y < bpoints[ utag ].pos.y ) {
                        bounds.max.y = bpoints[ utag ].pos.y;
                    }
                    
                }
                
            }
            
        }
        
    }

}

void ofApp::newAgent( string unique, DataSender & sender ) {

    if ( 
        Config::get()->names_filter.empty() || 
        Config::get()->names_filter.find( sender.name ) != std::string::npos ) {

        if ( sender.kind[ 3 ] ) {
            requests[ unique ].nexttime = ofGetElapsedTimeMillis();
            requests[ unique ].req = OSCAgent::makeKind( true, false, false, false );
        } else if ( sender.kind[ 4 ] ) {
            requests[ unique ].nexttime = ofGetElapsedTimeMillis();
            requests[ unique ].req.set( 4, true );
        }   
    
    } else {
        
        cout << "NOT sending request to " << unique << " called " << sender.name <<  " >> names filter: " << Config::get()->names_filter << endl;
        
    }
    
}

void ofApp::deletedAgent( string unique ) {

}    

//--------------------------------------------------------------
void ofApp::draw(){

    ofBackground( 25,25,25 );
    
    
    ofPushMatrix();
    ofTranslate( ofGetWidth() * 0.5, ofGetHeight() * 0.5, 0 );
    
//    ofRotateX( ( -ofGetMouseY() + ofGetHeight() * 0.5 ) * 0.1 );
//    ofRotateY( ( ofGetMouseX() - ofGetWidth() * 0.5 ) * 0.01 );
    ofRotateX( -barycenter.y * MapConfig::get()->rotation_mult );
    ofRotateY( barycenter.x * MapConfig::get()->rotation_mult );
    
    ofEnableDepthTest();
//    mapgrid.shaking();
    mapgrid.draw();
    ofDisableDepthTest();
    
    if ( MapConfig::get()->display_debug ) {
        
        ofSetLineWidth( 1 );
        ofSetColor( 255,0,0 );
        ofLine( 0,0,0, 100,0,0 );
        ofSetColor( 0,255,0 );
        ofLine( 0,0,0, 0,100,0 );
        ofSetColor( 0,0,255 );
        ofLine( 0,0,0, 0,0,100 );

        ofNoFill();
        ofSetColor( 255,255,0 );
        ofDrawRectangle(
            bounds.min.x, bounds.min.y, 0,
            bounds.max.x - bounds.min.x,
            bounds.max.y - bounds.min.y
        );

        ofSetColor( 255,255,255 );
        for ( map< string, BrushPoint >::iterator itp = bpoints.begin(); itp != bpoints.end(); ++itp ) {
            ofVec2f & v = itp->second.pos;
            ofCircle( v.x, v.y, 20 );
        }

    }
    
    ofPopMatrix();
    
    ofSetColor( 255,255,255 );
    
    if ( MapConfig::get()->display_debug ) {
    
        stringstream ss;
        ss << "brush: ";
        switch ( btype ) {
            case BRUSH_INFLATE:
                ss << "inflate";
                break;
            case BRUSH_PINCH:
                ss << "pinch";
                break;
            case BRUSH_PULL:
                ss << "pull";
                break;
            case BRUSH_PUSH:
                ss << "push";
                break;
            case BRUSH_PUSH_FORCE:
                ss << "force push";
                break;
            default:
                ss << "none";
                break;

        }
        ofDrawBitmapString( ss.str(), 10, 25 );
        if ( Config::get()->enable_osc ) {
            ofDrawBitmapString( oscagent->toString(), 10, 40 );
        }
        
    }
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    if ( !Config::get()->enable_osc ) {
        int t = ((int)  btype );
        t++;
        if ( t >= BRUSH_COUNT ) {
            t = 0;
        }
        btype = (BrushType) t;
    }
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}