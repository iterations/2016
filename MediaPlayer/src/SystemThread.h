/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   SystemThread.h
 * Author: frankiezafe
 *
 * Created on June 21, 2016, 4:07 PM
 */

#ifndef SYSTEMTHREAD_H
#define SYSTEMTHREAD_H

#include "ofMain.h"

class SystemThread : public ofThread {
    
public:
    
    SystemThread();
    virtual ~SystemThread();
    
    void start();
    void stop();
    void threadedFunction();
    
    // setters
    inline void setCommand( string start_command, string stop_command ) {
        start_cmd = start_command;
        stop_cmd = stop_command;
    }
    
private:
    
    string start_cmd;
    string stop_cmd;

};

#endif /* SYSTEMTHREAD_H */

