#include "ofMain.h"
#include "ofApp.h"
#include "ofAppNoWindow.h"
#include "Config.h"
#include "MediaConfig.h"

//#define NO_WIDOW 1

//========================================================================

int main( int argc, char *argv[] ) {

    std::cout << "MediaPlayer man" << std::endl;
    std::cout << "\t'-nonet' to disable network" << std::endl;
    std::cout << "\t'-n [string]' to set the name of the point" << std::endl;
    std::cout << "\t'-p [int]' to set local port, 23000 by default" << std::endl;
    std::cout << "\t'-b [int]' to set broadcast port, 20000 by default" << std::endl;
    std::cout << "\t'-s [int]x[int]' to set the camera resolution, 160x120 by default" << std::endl;
    std::cout << "\t'-t [int]' to set the threshold, 85 by default" << std::endl;
    std::cout << "\t'-f [string|string]' to define the names of points listened" << std::endl;
    std::cout << "\t'-m [int]' set to 1 to enable keyboard control" << std::endl;
    std::cout << "\t'-txt [string]' to define the text file to load, path relative to data/" << std::endl;
    std::cout << "\t'-sf [int]' to define font size" << std::endl;
    std::cout << "\t'-cf [int],[int],[int],[int]' to define font color (RGBA), alpha is optional" << std::endl;
    std::cout << "\t'-ct [int],[int],[int],[int]' to define comments color (RGBA), alpha is optional" << std::endl;
    std::cout << "\t'-ci [int],[int],[int]' to define (idle) background color when nothing is displayed" << std::endl;
    std::cout << "\t'-cb [int],[int],[int]' to define background color when media is playing" << std::endl;
    std::cout << "\t'-lock [int]' to define a lock time (in milliseconds) when a media is started, set to -1 to disable" << std::endl;
    std::cout << "\t'-verbose" << std::endl;
    
    for ( int i = 0; i < argc; ++i ) {
        
        std::string param = argv[i];
        
        if ( param.compare( "-p" ) == 0 && i < argc - 1 ) {
        
            int port = atoi( argv[i+1] );
            std::cout << "--local port: " << port << std::endl;
            Config::get()->osc_port = port;
        
        } else if ( param.compare( "-t" ) == 0 && i < argc - 1 ) {
        
            int threshold = atoi( argv[i+1] );
            std::cout << "--threshold: " << threshold << std::endl;
            Config::get()->threshold = threshold;
        
        } else if ( param.compare( "-b" ) == 0 && i < argc - 1 ) {
        
            int port = atoi( argv[i+1] );
            std::cout << "--broadcast port: " << port << std::endl;
            Config::get()->broadcast_port = port;
        
        } else if ( param.compare( "-n" ) == 0 && i < argc - 1 ) {
        
            std::cout << "--name: " << argv[i+1] << std::endl;
            Config::get()->name = argv[i+1];
        
        } else if ( param.compare( "-f" ) == 0 && i < argc - 1 ) {
        
            std::cout << "--filter: " << argv[i+1] << std::endl;
            Config::get()->names_filter = argv[i+1];
        
        } else if ( param.compare( "-s" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            // spliting the string on 'x'
            size_t xpos = param.find( "x" );
            if ( xpos != string::npos ) {
                Config::get()->cam_width = atoi( param.substr( 0, xpos ).c_str() );
                Config::get()->cam_height = atoi( param.substr( xpos + 1 ).c_str() );
                std::cout << "--size: " << Config::get()->cam_width << "x" << Config::get()->cam_height << std::endl;
            }
            
        } else if ( param.compare( "-verbose" ) == 0 ) {
        
            Config::get()->verbose = true;
            std::cout << "--verbose enabled" << std::endl;
        
        } else if ( param.compare( "-nonet" ) == 0 ) {
        
            Config::get()->enable_osc = false;
            std::cout << "--osc disabled" << std::endl;
        
        // MEDIA CONFIGURATIONS
        } else if ( param.compare( "-cf" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            vector<string> scol = MediaConfig::split( param, ',' );
            if ( scol.size() == 3 || scol.size() == 4 ) {
                MediaConfig::get()->font_red = atoi( scol[0].c_str() );
                MediaConfig::get()->font_green = atoi( scol[1].c_str() );
                MediaConfig::get()->font_blue = atoi( scol[2].c_str() );
                if ( scol.size() == 4 ) {
                    MediaConfig::get()->font_alpha = atoi( scol[3].c_str() );
                }
                std::cout << "--font_color: " << MediaConfig::get()->font_red << ", " << MediaConfig::get()->font_green << ", " << MediaConfig::get()->font_blue << ", " << MediaConfig::get()->font_alpha << std::endl;

            }
            
        } else if ( param.compare( "-ct" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            vector<string> scol = MediaConfig::split( param, ',' );
            if ( scol.size() == 3 || scol.size() == 4 ) {
                MediaConfig::get()->trace_red = atoi( scol[0].c_str() );
                MediaConfig::get()->trace_green = atoi( scol[1].c_str() );
                MediaConfig::get()->trace_blue = atoi( scol[2].c_str() );
                if ( scol.size() == 4 ) {
                    MediaConfig::get()->trace_alpha = atoi( scol[3].c_str() );
                }
                std::cout << "--trace_color: " << MediaConfig::get()->trace_red << ", " << MediaConfig::get()->trace_green << ", " << MediaConfig::get()->trace_blue << ", " << MediaConfig::get()->trace_alpha << std::endl;

            }
            
        } else if ( param.compare( "-cb" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            vector<string> scol = MediaConfig::split( param, ',' );
            if ( scol.size() == 3 ) {
                MediaConfig::get()->bg_red = atoi( scol[0].c_str() );
                MediaConfig::get()->bg_green = atoi( scol[1].c_str() );
                MediaConfig::get()->bg_blue = atoi( scol[2].c_str() );
                std::cout << "--background_color: " << MediaConfig::get()->bg_red << ", " << MediaConfig::get()->bg_green << ", " << MediaConfig::get()->bg_blue << std::endl;
            }
            
        } else if ( param.compare( "-ci" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            vector<string> scol = MediaConfig::split( param, ',' );
            if ( scol.size() == 3 ) {
                MediaConfig::get()->idle_red = atoi( scol[0].c_str() );
                MediaConfig::get()->idle_green = atoi( scol[1].c_str() );
                MediaConfig::get()->idle_blue = atoi( scol[2].c_str() );
                std::cout << "--idle_color: " << MediaConfig::get()->idle_red << ", " << MediaConfig::get()->idle_green << ", " << MediaConfig::get()->idle_blue << std::endl;
            }
            
        } else if ( param.compare( "-sf" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            MediaConfig::get()->font_size = atoi( param.c_str() );
            std::cout << "--font_size: " << MediaConfig::get()->font_size << std::endl;
            
        } else if ( param.compare( "-m" ) == 0 && i < argc - 1 ) {
            
            int m = atoi( argv[i+1] );
            if ( m == 1 ) {
                MediaConfig::get()->manual = true;
                std::cout << "--manual: enabled" << std::endl;
            }
            
        } else if ( param.compare( "-txt" ) == 0 && i < argc - 1 ) {
            
            param = argv[i+1];
            MediaConfig::get()->text_path = param;
            std::cout << "--text_path: " << MediaConfig::get()->text_path << std::endl;
            
        } else if ( param.compare( "-lock" ) == 0 && i < argc - 1 ) {
            
            MediaConfig::get()->lock_time = atoi( argv[i+1] );
            std::cout << "--lock_time: " << MediaConfig::get()->lock_time << std::endl;
            
        }
        
    }

#ifdef NO_WIDOW
    ofAppNoWindow window;
    ofSetupOpenGL( &window, 1920, 480, OF_WINDOW);
#else
    ofSetupOpenGL( 1024, 768, OF_FULLSCREEN );
#endif

    ofRunApp( new ofApp( ) );

}