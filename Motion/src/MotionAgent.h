/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   MotionAgent.h
 * Author: frankiezafe
 *
 * Created on June 22, 2016, 2:26 PM
 */

#ifndef MOTIONAGENT_H
#define MOTIONAGENT_H

#include "OSCAgent.h"

class MotionAgent : public OSCAgent {
public:
    
    MotionAgent( DataKind k ) : OSCAgent( k ) {}
    
    virtual void send( float presence );
    
private:

};

#endif /* MOTIONAGENT_H */

