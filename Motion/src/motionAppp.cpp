#include "motionApp.h"

using namespace ofxCv;
using namespace cv;

void motionApp::setup() {
    
    cam.setup( Config::get()->cam_width, Config::get()->cam_height );
    background.setLearningTime( MotionConfig::get()->learning_time );
    background.setThresholdValue( Config::get()->threshold );

    pxnum = -1;
    
    if ( Config::get()->enable_osc ) {
        DataKind k;
        k.set( 4, true );
        magent = new MotionAgent( k );
    } else {
        magent = nullptr;
    }
    
}

void motionApp::update() {

    if ( magent != nullptr ) {
        magent->update( ofGetElapsedTimeMillis() );
    }
    
    cam.update();

    if( cam.isFrameNew() ) {
        
        background.update( cam, thresholded );
        
        cv::Mat & fg = background.getForeground();
        if ( pxnum == -1 ) {
            pxnum = fg.cols * fg.rows * fg.dims;
            thresh = Config::get()->threshold;
        }
        
        int nonzeros = 0;
        for ( int i = 0; i < pxnum; ++i ) {
            if ( fg.data[i] > thresh ) { 
                nonzeros++;
            }
        }
        p = nonzeros * 1.f / pxnum;
        
#ifndef NO_WIDOW 
        thresholded.update();
#endif
        
        magent->send( p );
        
    }

}

void motionApp::draw() {

#ifdef NO_WIDOW 
    return;
#endif
    
    ofSetColor( 255,255,255 );
    cam.draw(0, 0);
    if(thresholded.isAllocated()) {
        thresholded.draw( cam.getWidth(), 0 );
    }
    ofSetColor( 0,0,0 );
    ofDrawBitmapString( ofToString( p ), 10, cam.getHeight() + 25 );
    
}

void motionApp::process( ofxOscMessage & msg ) {}
void motionApp::newAgent( string unique, DataSender & sender ) {}
void motionApp::deletedAgent( string unique ) {}