// pascale, entry vp
./MediaPlayer -n pascale.video -cb 180,180,180 -ct 0,0,0,0 -ci 255,255,0

// annie - motion and sound
./Motion -n annie-eye -l 2
#bugged
./MediaPlayer -n annie.sound -p 23001
#temp
omxplayer --loop of_v0.9.3_linuxarmv6l_release/apps/iterations2016/MediaPlayer/bin/data/sounds/* [tmp]

// femke - tags & sound
./Iteration -n femke-tag -s 320x240
#bugged
./MediaPlayer -n femke.sound -p 23028
#temp
omxplayer --loop of_v0.9.3/apps/iterations2016/MediaPlayer/bin/data/sounds/*

// francois
#map distortion
./bin/MapDistortion -norm 100 -z 200 -smooth 0.000001 -n fz.map -rad 200-300 -force 0.01-0.02 -chance 0.1 -norm 200 -wa 1-1000 -wf 0.001-3 -rot 0.02

// peter
# tracking

// rafaella
#floor vp
./MediaPlayer -n rafaella.video -p 23005 -cb 0,0,0 -ct 0,0,0,0 -ci 0,0,0 -lock 30000

// julien
./Iteration -n julien -s 320x240
./MediaPlayer -n julien.txt -p 23071 -txt iteration.txt -sf 150 -cf 0,0,0 -cb 180,180,180 -ct 0,0,0,0 -ci 255,255,0

// donatella
#iteration no window + web google map
./Iteration-nowindow -n donatella -s 640x480

// stefan
./MediaPlayer -n stefan.txt -p 23064 -txt iteration.txt -sf 150 -cf 0,0,0 -cb 180,180,180 -ct 0,0,0,0 -ci 255,255,0

