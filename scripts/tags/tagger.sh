#!/bin/bash

# to use this script, be sure you have the image >AllBchThinMarkers.png< next to the script
# there are 4096 tags available
# select the start and stop num, launch the script and print the pdf toprint/alltags.pdf

start_at=10
stop_at=30

# change at your own risk...

output_size=1024
margin_size=4
white_border=1
input_size=8

output_dir="toprint/"
output_path="tag_"
output_ext=".png"

counter=0

x_offset=0
y_offset=0

x_offset=$margin_size
y_offset=$margin_size

for y in $(seq 1 64); do 

	x_offset=$margin_size
	
		for y in $(seq 1 64); do 

		im_size=`expr $input_size + $white_border + $white_border`
		im_offsetx=`expr $x_offset - $white_border`
		im_offsety=`expr $y_offset - $white_border`


		if [[ $counter -ge $start_at ]] && [[ $counter -le $stop_at ]] 
		then 
			zeros=""
			if [ $counter -lt 10 ] 
			then
				zeros="000"
			elif [ $counter -lt 100 ] 
			then
				zeros="00"
			elif [ $counter -lt 1000 ] 
			then
				zeros="0"
			fi
			target_file=${output_dir}${output_path}${zeros}${counter}${output_ext}
			echo ${counter} :: ${target_file} :: "offset " ${x_offset}"x"${y_offset}
			convert "AllBchThinMarkers.png" -crop ${im_size}x${im_size}+${im_offsetx}+${im_offsety} -filter box -resize ${output_size}x${output_size} ${target_file}
		fi

		counter=`expr $counter + 1`

		x_offset=`expr $x_offset + $input_size + $margin_size`

	done

	y_offset=`expr $y_offset + $input_size + $margin_size`

done

sleep 1
convert -page A4 ${output_dir}*.png ${output_dir}alltags.pdf

echo "get the pdf in " ${output_dir}alltags.pdf
echo "done, have a nice day :)"

